/*jshint node: true*/
var express = require('express');
var http = require('http');
var path = require('path');
var app = express();
var less = require('less-middleware');
var fs = require('fs');

var words = fs.readFileSync('./odm2.txt', 'utf-8').toString().toLowerCase().split('\n');

app.configure(function () {
    app.set('port', process.env.PORT || 3000);
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(less({
        src: __dirname + '/public',
        compress: true
    }));
    app.use(express.static(path.join(__dirname, 'public')));
});

var server = http.createServer(app).listen(app.get('port'), function () {
    console.log("Serwer nasłuchuje na porcie " + app.get('port'));
});

var io = require('socket.io');
var socket = io.listen(server);

socket.configure( function() {
    socket.set('close timeout', 60 * 60 * 24); // 24h time out
    socket.set('heartbeat timeout', 60 * 60 * 24);
    socket.set('heartbeat interval', 60 * 60 * 24);
});

var deafaultRoom = "Node",
    roomNr = 1,
    alfa = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'w', 'z'],
    clients = {},
    j = 0,
    clientsPlayInRoom = {},
    clientsInRoom = [],
    wyniki = {};
    result = {};

var losowanie, odliczanie;

var dictionary = function(wyraz){
   // var ret = true; 
    var p = 0;
    var k = words.length-1;

    while (p <= k) {
        s = Math.floor((p+k)/2); // zaokrągalmy liczbę aby dostać część całkowitą
            if (words[s] === wyraz) {
                return false;
            }
            if (words[s] > wyraz) {
                k = s-1;
            }
            else {
                p = s+1;
            }
    }

        return false;
};
//   for(var i=0; i<words.length; i+=1){
//      if(words[i].match(wyraz)!==null){
//         ret = false;
//            break;
//       }
//    }

 //   return ret;
//};

var checkWord = function(word, letter){
    if(word===''){
        return true;
    }else if(word.substring(0,1).toLowerCase()!==letter){
        return true;
    }else if(dictionary(word.toLowerCase())){
        return true;
    }else{
        return false;
    }
};


var check = function(arg, lett){ 
    var result = [], 
        klucze = Object.keys(arg[0].odp); 
    klucze.forEach(function(value){
        arg.forEach(function(user){

            if(checkWord(user.odp[value].toLowerCase(), lett)){

                result.push({user: user.user, za: value, pkt: 0});
            }else{
                var max=0, mid=0;
                arg.forEach(function(user2){
                   
                    if(user.user!==user2.user){
                        if(checkWord(user2.odp[value].toLowerCase(), lett)){
                           
                            max+=1;
                        }
                        if(user.odp[value].toLowerCase()===user2.odp[value].toLowerCase()){
                       
                            mid+=1;
                        }
                    }
                });
                if(max===arg.length-1){
                   
                    result.push({user: user.user, za: value, pkt: 15});
                }else if(mid!==0){
                    
                    result.push({user: user.user, za: value, pkt: 5});
                }else{
                    
                    result.push({user: user.user, za: value, pkt: 10});
                }
            }
        });
    });
    return result;
};


var count = function(wyn, litera){  
    var obj = check(wyn, litera), 
        res = []; 

    obj.forEach(function(val, itr){
        if(res.length===0){
    
            res.push({user: val.user, pkt: val.pkt});
        }else{
            var flag = 0;
            res.forEach(function(val2){
                if(val2.user===val.user){
                
                    flag+=1;
                    val2.pkt = val2.pkt+val.pkt;
                }
            });
            if(flag===0){
               
                res.push({user: val.user, pkt: val.pkt});
            }
        }
    });
    return res;
};


var countAll = function(objOld, objNew, room){
    var obj = objOld[room];
    if(obj===undefined){ 
        objOld[room] = [];
        objNew.forEach(function(value){
            objOld[room].push(value);
        });
    }else{ 
        objOld[room].forEach(function(value){
            objNew.forEach(function(val){
                if(val.user===value.user)
                    value.pkt+=val.pkt;
            });
        });
    }

    return objOld;
};


socket.on('connection', function (client) {
    'use strict';

    var username, i=0, litera; 

   
    client.join(deafaultRoom);

    clients[client.id] = client;

   
    client.on('setUsername', function(msg){
        if(msg.username === '/admin'){             
        }else{          
            username = msg.username;
            client.username = username;
            client.emit('saveUsername', {server: 'Zapisano nowy nick!', rooms: socket.rooms});
      //      client.broadcast.to(deafaultRoom).emit('server', {server: 'New room!', rooms: socket.rooms});
        }
    });

  
    client.on('createRoom', function(msg){
       
        var nameRoom = 'room' + roomNr + '';
     
        client.leave(deafaultRoom);
        
        client.join(nameRoom);
        
        client.room = nameRoom;
        
        clientsInRoom[nameRoom] = []; 
        clientsInRoom[nameRoom].push(client.username);
        
        client.emit('saveRoom', {server: 'Utworzono pokój', clients: clientsInRoom[nameRoom]});
        
        client.broadcast.to(deafaultRoom).emit('server', {server: 'Nowy pokój!', rooms: socket.rooms});
    
        roomNr++;
    });

   
    client.on('joinMe', function(msg){
       
        client.leave(deafaultRoom);
        
        client.join(msg); 
        
        client.room = msg;
       
        clientsInRoom[msg].push(client.username);
       
        client.emit('joinRoom', {server: 'Znajdujesz się w pokoju: ' + msg, clients: clientsInRoom[msg], clientsPlay: clientsPlayInRoom[client.room]});
       
        client.broadcast.to(msg).emit('server', {server: 'Użytkownik: '+client.username+' dołączył do pokoju!', clients: clientsInRoom[msg], clientsPlay: clientsPlayInRoom[client.room]});       
    });

   
    client.on('leaveRoom', function(){
       
        if(clientsPlayInRoom[client.room]!==undefined){
            clientsPlayInRoom[client.room].forEach(function(value, index){
                if(value===client){
                    clientsPlayInRoom[client.room].splice(index, 1);
                }
            });
        }
        
        clientsInRoom[client.room].forEach(function(value, index){
            if(value===client.username){
                clientsInRoom[client.room].splice(index, 1);
            }
        });

        var oldRoom = client.room;

        client.leave(client.room);
        client.join(deafaultRoom);
        client.room = deafaultRoom;
        client.emit('leaveRoom', {server: 'Opuściłeś pokój: ' + oldRoom, rooms: socket.rooms});
        client.broadcast.to(oldRoom).emit('server', {server: 'Użytkonik: ' +client.username+ ' opuścił pokój!', clients: clientsInRoom[oldRoom], clientsPlay: clientsPlayInRoom[client.room]});
    });

    client.on('joinGame', function(msg){
        
        if(clientsPlayInRoom[client.room]!==undefined){
            clientsPlayInRoom[client.room].push(client.username);
        }else{
            clientsPlayInRoom[client.room] = [];
            clientsPlayInRoom[client.room].push(client.username);
        }

        
        if(clientsPlayInRoom[client.room].length===2){
            clients[socket.rooms['/'+client.room][i]].emit('losujesz', {msg: 'Losujesz!', clientsPlay: clientsPlayInRoom[client.room]});
            clients[socket.rooms['/'+client.room][i]].broadcast.to(client.room).emit('losowanie', {server: 'Klient: ' + clients[socket.rooms['/'+client.room][i]].username + ' losuje', clientsPlay: clientsPlayInRoom[client.room]});
            losowanie = setInterval(function(){
                console.log(j);
                if(j===alfa.length){
                    j=0;
                }
                j++;
            }, 1000);
        }else{
           
            client.emit('wait', {msg: 'Przyjąłem! Musisz poczekać na więcej graczy.', clientsPlay: clientsPlayInRoom[client.room]});
            clients[socket.rooms['/'+client.room][i]].broadcast.to(client.room).emit('wait', {msg: 'Kolejny gracz dołączyl! Już co raz bliżej rozpoczęcia rozgrywki', clientsPlay: clientsPlayInRoom[client.room]});
        }
        
    });

    
    client.on('stop', function(msg){
        clearInterval(losowanie);
        litera = alfa[j];
        client.emit('start', alfa[j]);
        client.broadcast.to(client.room).emit('start', alfa[j]);
    });

   
    client.on('sendWynik', function(msg){
   //     var index = '/'+client.room; 

        var obj = {                
            user: client.username,
            odp: msg
        };

        
        if(wyniki[client.room]===undefined || wyniki[client.room].length===0){ 

            wyniki[client.room] = []; 
            wyniki[client.room].push(obj);

            var sec = 10;

            
            odliczanie = setInterval(function(){
                client.emit('timer', {time: sec});
                client.broadcast.to(client.room).emit('timer', {time: sec});
                if(sec===0){
                    
                    client.broadcast.to(client.room).emit('koniecCzasu');
                    clearInterval(this);
                }
                sec--;
            }, 1000);

        }else{
            
            wyniki[client.room].push(obj);
            client.emit('mamWyniki');
            
            if(wyniki[client.room].length===clientsPlayInRoom[client.room].length){ 
               
                clearInterval(odliczanie); 
                client.emit('sprawdzanie', 'Trwa sprawdzanie wyników');
                client.broadcast.to(client.room).emit('sprawdzanie', 'Trwa sprawdzanie wyników');
                console.log("\n\n\n\n");
                console.log("Tu będe sprawdzał wyniki");
                console.log("\n\n\n\n");
                var obj = count(wyniki[client.room], alfa[j]);
                var wynikiNoSort = countAll(result, obj, client.room);
                var wynikiSort = wynikiNoSort[client.room].sort(function(a,b){ 
                    if(a.pkt > b.pkt){
                        return -1;
                    }
                    if(a.pkt < b.pkt){
                        return 1;
                    }
                    return 0;
                });

                var objSort = obj.sort(function(a,b){ 
                    if(a.pkt > b.pkt){
                        return -1;
                    }
                    if(a.pkt < b.pkt){
                        return 1;
                    }
                    return 0;
                });
         
                client.emit('wyniki', {wyniki: wynikiSort, runda: objSort});
                client.broadcast.to(client.room).emit('wyniki', {wyniki: wynikiSort, runda: objSort});
 
                wyniki[client.room] = [];

                var delay = 15;

                setInterval(function(){
                    if(delay===0){
                        if(clientsPlayInRoom[client.room].length===2){
                            clients[socket.rooms['/'+client.room][i]].emit('losujesz', {msg: 'Losujesz!', clientsPlay: clientsPlayInRoom[client.room]});
                            clients[socket.rooms['/'+client.room][i]].broadcast.to(client.room).emit('losowanie', {server: 'Klient: ' + clients[socket.rooms['/'+client.room][i]].username + ' losuje', clientsPlay: clientsPlayInRoom[client.room]});
                            losowanie = setInterval(function(){
                                console.log(j);
                                if(j===alfa.length){
                                    j=0;
                                }
                                j++;
                            }, 1000);
                        }else{
                            client.emit('wait', {msg: 'Przyjąłem! Musisz poczekać na więcej graczy.', clientsPlay: clientsPlayInRoom[client.room]});
                        }
                        clearInterval(this);
                    }
                    delay-=1;
                }, 1000);

               if (i === clientsPlayInRoom[client.room].length-1) { 
                i = 0;
                } 

                else {
                    i+=1;
                }       //if(i===3) i=0 else i+=1

            }
        }
    });

});