/*jshint node: true, browser: true, jquery: true */
/*global io: false */
$(document).ready(function(){

    'use strict';
    var socket = io.connect(),  
        login = $('#login'),
        rooms = $('#rooms'),
        game = $('#game'),
        footer = $('#footer .container p'),
        sendResult = $('#sendResult'),
        flagaWyslane = false; //
    
    var okienko = function(modalId){
        var sec = 3;
        modalId.modal({backdrop: 'static'}, 'show');
        var timer = setInterval(function(){
            if(sec===0){
                modalId.modal('hide');
                clearInterval(timer);
            }
            sec-=1;
        }, 500);
   };

    rooms.hide();
    game.hide();
    $('#form').hide();

    $('#login input').attr('disabled', 'disabled');  
    
    footer.append('Trwa łączenie &nbsp; <img src="http://www.gdansk4u.pl/files/images/loading.gif" height="10">');

    /*=== Eventy z servera ===*/

    //Polacznie z serverem
    socket.on('connect', function () {

        $('#login input').removeAttr('disabled');
     
        footer.html('Połączony <i class="icon-ok"></i>');
        console.log('Połączony!');
    });

    /*=== Ustawianie nicka - Logowanie do gry ===*/

    socket.on('saveUsername', function(msg){
 
        footer.html(' ');

        footer.append(msg.server);

        login.hide();
  
        rooms.show();

        $('#rooms table tbody').html(' ');

        $.each(Object.keys(msg.rooms), function(index, value){

            if(value!=='' && value!=='/Node'){

                $('#rooms table tbody').append('<tr><td>'+value.substring(1, value.length)+'</td><td><button id="'+value+'" class="dolacz btn btn-success">Dołącz</i></button></td></tr>');
            }
        });
    });

    /*=== Operacje zwiazne z pokojami ==*/

    socket.on('saveRoom', function(msg){
        footer.html(' ');
        footer.append(msg.server);
        rooms.hide();
        game.show();
        if(msg.clients!==undefined){
            $('#users').html(' ');
            $.each(msg.clients, function(index, value){
                $('#users').append('<li>'+value+'</li>');
            });
        }
    });

    socket.on('joinRoom', function(msg){
        rooms.hide();
        game.show();
        footer.html(' ');
        footer.append(msg.server);
        if(msg.clients!==undefined){
            $('#users').html(' ');
            $.each(msg.clients, function(index, value){
                $('#users').append('<li>'+value+'</li>');
            });
        }
        $('#players').html(' ');
        $.each(msg.clientsPlay, function(index, value){
            $('#players').append('<li>'+value+'</li>');
        });
    });

    socket.on('leaveRoom', function(msg){
        game.hide();
        rooms.show();
        footer.html(' ');
        footer.append(msg.server);
        $('#rooms table tbody').html(' ');
        $.each(Object.keys(msg.rooms), function(index, value){
            console.log(value);
            if(value!=='' && value!=='/Node'){
                $('#rooms table tbody').append('<tr><td>'+value.substring(1, value.length)+'</td><td><button id="'+value+'" class="dolacz btn btn-success">Dolacz</i></button></td></tr>');
            }
        });
 //       $('#players').html(' ');
  //      $.each(msg.clientsPlay, function(index, value){
  //          $('#players').append('<li>'+value+'</li>');
   //     });

    });

    /* Operacje z samej gry - juz w pokoju */

    //Wyswietlanie czasu do konca tury
    socket.on('timer', function(msg){
        $('#time').html(' ');
        console.log(msg.time);
        $('#time').text(msg.time);
    });

    socket.on('losujesz', function(msg){
        flagaWyslane = false;
        $('#sendResult').removeAttr('disabled');
        $('#wait').modal('hide');
        $('#sayStop').modal({backdrop: false}, 'show');
        $('#players').html(' ');
        $.each(msg.clientsPlay, function(index, value){
            $('#players').append('<li>'+value+'</li>');
        });
      //    okienko($('#sayStop'));
        $('#joinGame').hide();
    });

    //Odebranie informacji o tym ze inny user losuje
    socket.on('losowanie', function(msg){
        flagaWyslane = false;
        $('#sendResult').removeAttr('disabled');
        $('#wait').modal('hide'); 
        $('#stopLetter .modal-body').text(msg.server);
        $('#stopLetter').modal({backdrop: false}, 'show');        
        $('#players').html(' ');
        $.each(msg.clientsPlay, function(index, value){
            $('#players').append('<li>'+value+'</li>');
        });
         okienko($('#stopLetter'));
        $('#joinGame').hide();
    });

    //Wiadomosc ze trzeba poczekac na innych graczy
    socket.on('wait', function(msg){
        $('#wait modal-body').text(msg.msg);
        $('#players').html(' ');
        $.each(msg.clientsPlay, function(index, value){
            $('#players').append('<li>'+value+'</li>'); // inny użytkownik losuje
        });
        okienko($('#wait'));
    });

    //po wylosowaniu litery start gry
    socket.on('start', function(msg){
        $('#sayStop').modal('hide');
        $('#stopLetter').modal('hide');
        $('#joinGame').hide();
        $('#letter').html(' ');
        $('#letter').append(msg.toUpperCase());
        $('#form').show();
        $('input[type="text"]').removeAttr('disabled');
    });

    //potwierdzenie otrzymania wynikow (falgaWyslane jest potrzebna do automatycznego pobieranie wynikow
    // jesli flaga jest na true to znaczy ze wyniki juz poszly i nie trzeba od tego klienta zbierac wynikow)
    socket.on('mamWyniki', function(){
        flagaWyslane = true;
    });

    //automatyczne zbieranie wynikow po uplynieciu czasu 10s.
    socket.on('koniecCzasu', function(){
        if(!flagaWyslane){
            var obj = {
                panstwo: $('#panstwo').val(),
                miasto: $('#miasto').val(),
                zwierze: $('#zwierze').val(),
                roslina: $('#roslina').val(),
                rzecz: $('#rzecz').val(),
                imie:$ ('#imie').val()
            };
            socket.emit('sendWynik', obj);
        }
    });

    //Informacja od server ze teraz bedzie sprawdzal (sprawdzanie moze chwile trwac)
    socket.on('sprawdzanie', function(msg){
        $('#time').html(' '); 
        $('#checking').modal({backdrop: 'static'}, 'show');
    });

    //odebranie wynikow od serwera
    socket.on('wyniki', function(msg){
        $('#checking').modal('hide');
        $('#wyniki').html(' ');
        $.each(msg.wyniki, function(index, value){
            var pos = index+1;
            $('#wyniki').append('<li>'+pos+'. '+value.user+': '+value.pkt+' pkt |</li>');
        });
        //runda
        var ol = '<li><ul>';
        $.each(msg.runda, function(index, value){
            ol+='<li>'+value.user+': '+value.pkt+'</li>';
        });
        ol+='</ul></li>';
        $('#runda').append(ol);
        //czyszczenie formularza gry
        $('input[type="text"]').val('');
    });

    //Wiadomosci z servera
    socket.on('server', function(msg){
        if(msg.server!==undefined){
            footer.html(' ');
            footer.append(msg.server);
        }
        if(msg.rooms!==undefined){
           $('#rooms table tbody').html(' ');
            $.each(Object.keys(msg.rooms), function(index, value){
                console.log(value);
                if(value!=='' && value!=='/Node'){
                    $('#rooms table tbody').append('<tr><td>'+value.substring(1, value.length)+'</td><td><button id="'+value+'" class="dolacz btn btn-success">Dolacz</button></td></tr>');
                }
            });
        }
        console.log(msg.clients);
        if(msg.clients!==undefined){
            $('#users').html(' ');
            $.each(msg.clients, function(index, value){
                $('#users').append('<li>'+value+'</li>');
            });
        }
        if(msg.clientsPlay!==undefined){
            $('#players').html(' ');
            $.each(msg.clientsPlay, function(index, value){
                $('#players').append('<li>'+value+'</li>');
            });    
        }
        

    });

    //Zdarzenie na klikniecie logowania
    $('#login button').click(function(){
        //wyslanie username do serwera
        if($('#appendedPrependedInput').val().length<1){
            alert("Nazwa użytkownika nie może być pusta");
        }else{
            socket.emit('setUsername', {username: $('#login input').val()});
            $('#login input').attr('disabled', 'disabled');    
        }
    });

    //Zdarzenie na utworzenie pokoju
    $('#createRoom').click(function(){
        //wyslanie do serwera prosby o utworzenie pokoju
        socket.emit('createRoom');
    });

    //Zdarzenie na dolaczenie do pokoju
    $(document).click(function(event){
        /*
            1. Klikamy w caly dokument html (to u gory)
            2. Sprawdzamy czy w to co kliknelismy (tam gdzie byl kursor myszy to jest obiekt z class="dolacz")
            3. Jesli tak to wysylamy zadanie do serwera z prosba o dolaczenie do danego pokoju
                nazwa pokoju jest w id, tylko ze id jest takie id="/room1" i musimy wyciac ten pierwszy znak
        */
        if($(event.target).attr('class').match("dolacz")!==null){
            socket.emit('joinMe', $(event.target).attr('id').substring(1, $(event.target).attr('id').length));
        }
    });

    //Opuszczanie pokoju
    $('#leaveRoom').click(function(){
        //wyslanie do serwera informacji o tym ze opuszczamy pokoj
        socket.emit('leaveRoom');
    });

    $('#sendResult').click(function(){ // wysłanie wyników przed czasem
        //Zbieranie wynikow z formularza
        var obj = {
            panstwo: $('#panstwo').val(),
            miasto: $('#miasto').val(),
            zwierze: $('#zwierze').val(),
            roslina: $('#roslina').val(),
            rzecz: $('#rzecz').val(),
            imie:$ ('#imie').val()
        };
        //wyslanie wynikow do serwera za pomoca guzika
        socket.emit('sendWynik', obj);
        //wylaczamy guzik zeby 2 razy tego nie wysylac
        $('#sendResult').attr('disabled', 'disabled');
    });

    //Przylaczanie do gry
    $('#joinGame').click(function(){
        socket.emit('joinGame'); // nie byłem pewny czy można zrobić samo joinGame , ''
        $('#joinGame').hide();
    });

    //Stopowanie litery
    $('#sayStop #stop').click(function(){
        socket.emit('stop');
    });


});